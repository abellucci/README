---
layout: markdown_page
title: "Alana Bellucci's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY Q3 2022

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Onboarding | General | 25% |
| Onboarding | Product Management Specific | 25% |
| Monitor | Getting familiar with GitLab and Monitor | 30% |
| Synchronous Meetings | Coffee Chats, Team Meetings, AMA | 10% |
| Personal Development | Writing, Reading, Learning | 10% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

Priorities are set on Monday and reviewed at EOD on Fridays:
 ✅ Successfully completed
 ❌ Did not complete

## Week of 2021-10-25
### Priorities
1. ❌ Complete near team, mid-term and long term strategy for Monitor, [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92266), _Continued to iterate and am getting closer.  Still need to update Dogfooding section and review how we are measuring success._
1. ✅ Update the [Monitor - Quarterly Direction (Epics) Board](https://gitlab.com/groups/gitlab-org/-/epic_boards/9486?label_name[]=group::monitor) for the next three quarters, FY22 Q4, FY23 Q1, and FY23 Q2.  _Still a WIP._
1. ✅ [Incident Management Channel TechChat Series](https://gitlab.com/gitlab-com/channel/channels/-/issues/511)
1. ✅ Finish interviews for [Slack and incident management workflow problem validation](https://gitlab.com/gitlab-org/gitlab/-/issues/333030) and review insights.
1. ✅ Review Incident Management Blog post, [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92290)
1. ✅  Week 1 of Opportunity Mapping Course

## Week of 2021-10-18
### Priorities
1. ❌ Complete near team, mid-term and long term strategy for Monitor _Still a WIP._
1. ❌ Update the [Monitor - Quarterly Direction (Epics) Board](https://gitlab.com/groups/gitlab-org/-/epic_boards/9486?label_name[]=group::monitor) for the next three quarters, FY22 Q4, FY23 Q1, and FY23 Q2.  _Still a WIP._
1. ✅ [Monitor PI Review : October](https://gitlab.com/gitlab-com/Product/-/issues/3246)
1. ✅ Create first draft for the [Incident Management Channel TechChat Series](https://gitlab.com/gitlab-com/channel/channels/-/issues/511)
1. ✅ [Monitor: Bi-Monthly Internal Customer Call](https://docs.google.com/document/d/1amfHJ6sgPJB3RxOysi2AKonFqYM2vIGBAe_NrDd8uzo/edit#heading=h.suq5u0diw2ni)
1. ✅ [14.4 Monitor:Monitor retrospective](https://gitlab.com/gl-retrospectives/monitor/monitor/-/issues/21)
1. ✅ Interviews for [Slack and incident management workflow problem validation](https://gitlab.com/gitlab-org/gitlab/-/issues/333030)
1. ✅ Week 5 of Continuous Interviewing Course


## Week of 2021-10-11
### Priorities
1. ✅ Finalize [Planning Issue for 14.5](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/81)
1. ✅ Record Kickoff [video](https://www.youtube.com/watch?v=tt-afgro3gU) for 14.5 Planning
1. ❌ Organize the [Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) Epic and outline of the Roadmap through the end of FY22.  (Got a good start with setting up boards, but DNC.)
1. ✅ Finalize [Release Post Item](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90544) for 14.4
1. ✅ Interviews for [Slack and incident management workflow problem validation](https://gitlab.com/gitlab-org/gitlab/-/issues/333030)
1. ✅ Week 4 of Continuous Interviewing Course

## Week of 2021-10-04
### Priorities
1. ✅ [Direction Updates Ops: September](https://gitlab.com/gitlab-com/Product/-/issues/3132)
1. ✅ Host the first Monitor: Monthly Internal Customer Call
1. ✅ Week 3 of Continuous Interviewing Course
1. ✅ Q3 CAB
1. ✅ Start the Planning Issue for 14.5
1. ❌ Organize the [Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) Epic and outline of the Roadmap through the end of FY22.
1. ✅ Iterate on [Monitor's Tiered Pricing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90075)


## Week of 2021-09-27
### Priorities
*On PTO from 2021-09-27 until 2021-09-30*
1. ✅ Week 2 of Continuous Interviewing Course
1. ✅ Q3 CAB- Dry run (full run through)
1. ✅ Catchup as much as possible before the weekend

## Week of 2021-09-20
### Priorities
1. ❌ Organize the [Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) Epic and outline of the Roadmap through the end of FY22.  
1. ❌ [Direction Updates Ops: September](https://gitlab.com/gitlab-com/Product/-/issues/3132)
1. ✅ [14.3 Monitor:Monitor retrospective](https://gitlab.com/gl-retrospectives/monitor/monitor/-/issues/20)
1. ❌ Finalize [Monitor's Tiered Pricing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90075)
1. ✅ Prep and deliver GitLab Incident Management Demo
1. ✅ Review and test existing [Slack notifications and integration](https://docs.gitlab.com/ee/operations/incident_management/paging.html#slack-notifications) 
1. ✅ Week 1 of Continuous Interviewing Course
1. ✅ Finalize [PM Coverage for Alana Bellucci from 2021-09-27 until 2021-09-30](https://gitlab.com/gitlab-com/Product/-/issues/3127) issue
1. ❌ Complete section 3 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/) 

## Week of 2021-09-13
### Priorities
1. ❌ Organize the [Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) Epic and outline of the Roadmap through the end of FY22.  (_I made a lot of progress, but this is turning out to be an iterative process._)
1. ✅ Finalize the Planning Issue for 14.4 and record the kickoff video
1. ✅ Make sure 14.3 Release Post Items are merged
1. ✅ [Ops Section PI Review : September](https://gitlab.com/gitlab-com/Product/-/issues/3113)
1. ❌ Finalize [Monitor's Tiered Pricing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/90075)
1. ✅ Meet with Tristan to understand Monitor features outside of Incident Management
1. ✅ Start work on [Problem validation: Slack and incident management workflows](https://gitlab.com/gitlab-org/gitlab/-/issues/333030)
1. ✅ Prep for Continuous Interviewing course that starts next week!
1. ✅ Shadow a Deep Dive interview as a part of Interview Training.
1. ❌ Complete section 3 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/) 


## Week of 2021-09-07
### Priorities
1. ❌ Organize the [Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) Epic and outline of the Roadmap through the end of FY22.  (_Got started, but did not finish._)
1. ✅ Start the Planning Issue for 14.4
1. ✅ Start 14.3 Release Post Items
1. ✅ Update the [pricing section](https://about.gitlab.com/direction/monitor/#pricing) on the Product Direction Page for Monitor, specifically for Incident Management.
1. ✅ Shadow a Deep Dive interview as a part of Interview Training.
1. ✅ Coffee Chat with Chris Balane, the new Release PM.
1. ✅ Attend one or more Incident Management Usability sessions with Amelia.
1. ❌ Complete section 3 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/) 


## Week of 2021-08-30
### Priorities
1. ❌ Organize the [Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) Epic and outline of the Roadmap through the end of FY22.  (_Moving to next week to complete alongside the 14.4 Planning Issue._)
1. ✅ Coffee Chat with John Jarvis (_Setup recurring monthly call._)
1. ✅ 360 Reviews
1. ❌ Complete section 3 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)  (_Reverted to a previous commit and pipelines are now passing!_)
1. ✅ Attend one or more Incident Management Usability sessions with Amelia
1. ✅ Demo On-Call Schedule Management to an existing GitLab Premium Customer and document the insights in DoveTail.
1. ✅ Finish all pieces of [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1287) I can complete independently and reach out to the Product team to see if I can shadow an interview.
1. ✅ Email select members from the original Special Interest Group (SIG) for Monitor.
1. ✅ Started the UX Shadow Program

# Week of 2021-08-23
### Priorities
1. ✅ Continue to organize epics, backlog and issues.
2. ❌ Work with internal customers to setup `Monitor:Monthly Internal Stakeholder Call`  (_The Infrastructure team has a lot on their plate.  Instead, I setup a coffee chat for next with John Jarvis.  He is assigned to be the Monitor SRE champion._)
3. ❌ Complete section 3 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)  (_I ran into some issues in this section and will need to rebase to a working state._)
4. ✅ Review and complete all tasks for the [Monitor Stage Onboarding Issue](https://gitlab.com/gitlab-org/monitor/onboarding/-/issues/23).
5. ✅ Prep demo of On-Call schedule management and escalation policies for upcoming customer meetings.

## Week of 2021-08-16
### Priorities
1. ✅ Merge RPIs for 14.2
2. ✅ Open and MR for updating [August PI's](https://gitlab.com/gitlab-com/Product/-/issues/2981)
3. ✅ Complete section 2 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)
4. ❌ Attend Iteration Office hours to see how to best iterate on permissions moving forward.  (_Iteration Office Hours were cancelled this month._)
5. ❌ Reach out to at least two non-GitLab Monitor customers.  (_Decided to start organizing Epics and the backlog before reaching out to customers.  Two customer meetings were scheduled by TAM's within the next month._)

## Week of 2021-08-09
### Priorities
1. ✅ Work with team members to make sure we are all set for the [Headcount Reset](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/76#note_646944671)
2. ✅ Finalize RPIs for 14.2.
3. ❌ Complete sections 2, 3, and 4 of the [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)
4. ❌ Continue to test workflows for Monitor so I understand what's already been built.   
5. ✅ Finish and close my [GitLab Onboarding Issue](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2917)
6. ✅ 14.3 Milestone Kickoff video

## Week of 2021-08-02
### Priorities
1. ✅ Work with Crystal to finalize the plan for 14.3 - _There will be some changes next week with the work the team will be doing for Infradev._
2. ❌ Continue to test workflows for Monitor so I understand what's already been built.  
3. ✅ Have a coffee chat with two members from the Infrastructure team. - _Met with Dave Smith and Brent Newton_
4. ✅ Meet with the co-founder of Rootly. - Co-founder of Rootly cancelled the demo, but I did some [competitive analysis](https://gitlab.com/gitlab-com/Product/-/issues/2943) and recorded a [Speed Run](https://www.youtube.com/watch?v=2ufjstRlqbM).
5. ✅ Get through 300 of 325 from my [GitLab Onboarding Issue](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2917)


