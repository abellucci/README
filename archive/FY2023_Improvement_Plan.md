---
layout: markdown_page
title: "Alana Bellucci's Quarterly Improvement Plan"
---

# Quarterly Improvement Objectives:
This content is meant to communicate what I am actively doing to improve, learn and study for the quarter.

 ✅ Successfully completed
 ❌ Did not complete
 🟡 Partially completed

## Q4 FY2023
Become a better story teller, writer and speaker.
### Books
* ✅  [A Compass For The Heart](https://thestoryoftelling.com/books/compass-for-the-heart/)
* ✅ [Story Driven](https://thestoryoftelling.com/books/story-driven/)
### Courses
* ❌ [The Story of Telling](https://thestoryoftelling.com/story-skills/)
* ❌ [Crucial Conversations Training](https://about.gitlab.com/handbook/people-group/learning-and-development/learning-initiatives/crucial-conversations/)
* ❌ [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)

## Q3 FY2023
### Courses
* ✅ [Communicate to Influence](https://decker.com/courses/communicate-to-influence/) course

## Q2 FY2023
On parental leave, did not set improvement plan.

## Q1 FY2023
### Courses
* 🟡 [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)
* ✅ [Ally Training](https://about.gitlab.com/handbook/communication/ally-resources/#ally-training)
