---
layout: markdown_page
title: "Alana Bellucci's Quarterly Improvement Plan"
---

# Quarterly Improvement Objectives:
This content is meant to communicate what I am actively doing to improve, learn and study for the quarter.

 ✅ Successfully completed
 ❌ Did not complete
 🟡 Partially completed


## Q4 FY2022

### Courses
* ✅ [Opportunity Mapping](https://learn.producttalk.org/p/opportunity-mapping) 
* 🟡 [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)
* ✅ [Ally Training](https://about.gitlab.com/handbook/communication/ally-resources/#ally-training)
* ✅ [GitLab Learn training on Iteration](https://gitlab.com/gitlab-com/Product/-/issues/3241)

___

## Q3 FY2022

### Books
* 🟡 [Human Kind - A Hopeful History](https://www.amazon.com/Humankind-Hopeful-History-Rutger-Bregman/dp/0316418536) | Rutger Bregman
* 🟡 [Continuous Discovery Habits: Discover Products that Create Customer Value and Business Value](https://www.amazon.com/Continuous-Discovery-Habits-Discover-Products/dp/1736633309) | Teresa Torres

### Courses
* ✅ [Continuous Interviewing](https://learn.producttalk.org/p/continuous-interviewing) 
* 🟡 [GitLab CI course](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)