---
layout: markdown_page
title: "Alana Bellucci's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY Q4 2022

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Monitor | Incident Management specific | 60% |
| Monitor | Getting familiar with other categories and stages at GitLab | 10% |
| Synchronous Meetings | Coffee chats, team meetings, AMA | 10% |
| Sensing Mechanisms | Customer interviews, competition | 10% |
| Personal Development | writing, reading, learning | 10% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

Priorities are set on Monday and reviewed at EOD on Fridays:
 - ✅ Successfully completed
 - ❌ Did not complete
 - 🟡 Partially completed

## Week of 2022-01-24
### Priorities
1. ✅ Lead the [Continuous Verification Opportunity Canvas](https://gitlab.com/gitlab-org/gitlab/-/issues/349350) review
1. ✅ Start the Continuous Verification direction page 
1. ❌ Finalize and merge [Incident Management features the Infrastructure team is currently dogfooding](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97289)
1. ✅ Finalize and merge all MRs related to [Updating Product Onboarding Templates](https://gitlab.com/gitlab-com/Product/-/issues/3357)
1. ✅ Finalize MRs for [Refreshing Category Visions](https://gitlab.com/gitlab-com/Product/-/issues/3611)

## Week of 2022-01-17
### Priorities
_Four day week with MLK Day on Monday_
1. ✅ Finalize the [Continuous Verification Opportunity Canvas](https://gitlab.com/gitlab-org/gitlab/-/issues/349350) 
1. ✅ Finalize questions for the [Research Request: Continuous Verification](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5885)
1. ✅ Refine goals and start the discussion guide for [Problem Validation: Continuous Verification](https://gitlab.com/gitlab-org/ux-research/-/issues/1748)
1. ✅ Complete the [Ops Section PI Review : January](https://gitlab.com/gitlab-com/Product/-/issues/3695)
1. ✅ Finalize name change steps for [Renaming Monitor:Monitor to Monitor:Respond](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/97)

## Week of 2022-01-10
### Priorities
1. ✅ Merge MR's for documentation changes and deprecations for [15.0 Monitor Stage Deprecations](https://gitlab.com/groups/gitlab-org/-/epics/7188)
1. ✅ Complete the first draft of the [Continuous Verification Opportunity Canvas](https://gitlab.com/gitlab-org/gitlab/-/issues/349350) 
1. 🟡 Work through name change steps for [Renaming Monitor:Monitor to Monitor:Respond](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/97)

## Week of 2022-01-03
### Priorities
_Four day week with FF on Monday_
1. ✅  MR's for documentation changes and deprecations for [15.0 Monitor Stage Deprecations](https://gitlab.com/groups/gitlab-org/-/epics/7188) are `Ready` for review
1. 🟡 Start the [Continuous Verification Opportunity Canvas](https://gitlab.com/gitlab-org/gitlab/-/issues/349350) 
1. ❌ Finalize the requirements for additional [Error Tracking Instrumentation ](https://gitlab.com/gitlab-org/gitlab/-/issues/345053)
1. ✅ [Ops Section Direction Updates : December](https://gitlab.com/gitlab-com/Product/-/issues/3587)

## Week of 2021-12-27
### Priorities
_Three day week with FF on Monday and NYE on Friday_
1. ✅ Open MR's for documentation changes and deprecations for [15.0 Monitor Stage Deprecations](https://gitlab.com/groups/gitlab-org/-/epics/7188)
1. 🟡 Open MR's for Continuous Verification category and direction - [issue link](https://gitlab.com/gitlab-com/Product/-/issues/3577), _working on the [Continuous Verification Opportunity Canvas](https://gitlab.com/gitlab-org/gitlab/-/issues/349350) prior to creating the category and direction.
1. ❌ Finalize the requirements for additional [Error Tracking Instrumentation ](https://gitlab.com/gitlab-org/gitlab/-/issues/345053)
1. ✅ Complete [2022Q4 - Training Courses](https://gitlab.com/gitlab-com/Product/-/issues/3537)
1. ✅ Iterate on MR's for [Update Product Onboarding Templates](https://gitlab.com/gitlab-com/Product/-/issues/3357)

## Week of 2021-12-20
### Priorities
_Four day week with Christmas Eve on Friday_
1. ✅ Finalize [Opportunity Canvas for Error Tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/345058) for review by Anoop and Scott.
1. ❌ Finalize the requirements for additional [Error Tracking Instrumentation ](https://gitlab.com/gitlab-org/gitlab/-/issues/345053)
1. ✅ Iterate on MR's for [Update Product Onboarding Templates](https://gitlab.com/gitlab-com/Product/-/issues/3357)
1. ❌ Open MR's for documentation changes for [15.0 Monitor Stage Deprecations](https://gitlab.com/groups/gitlab-org/-/epics/7188)
1. ✅ Start [2022Q4 - Training Courses](https://gitlab.com/gitlab-com/Product/-/issues/3537)


## Week of 2021-12-13
### Priorities
1. ✅ Iterate on the [Opportunity Canvas for Error Tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/345058)
1. 🟡 Finalize the requirements for additional [Error Tracking Instrumentation ](https://gitlab.com/gitlab-org/gitlab/-/issues/345053)
1. ✅ Finalize Release Post Items for 14.6
1. ✅ Start the [Monitor 14.7 Planning Issue](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/94)
1. ✅ Iterate on MR's for [Update Product Onboarding Templates](https://gitlab.com/gitlab-com/Product/-/issues/3357)

## Week of 2021-12-06
### Priorities
1. ✅ Start the [Opportunity Canvas for Error Tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/345058)
1. ❌ Complete the requirements [Error Tracking Instrumentation ](https://gitlab.com/gitlab-org/gitlab/-/issues/345053)
1. ❌ Complete speed runs for Void and Lightstep Incident Response 
1. ✅ Finalize the [UX Roadmap: Incident Management Viable -> Complete](https://gitlab.com/groups/gitlab-org/-/epics/7013)
1. ✅ Product Team Ally Training
1. ✅ Monitor Team [Lighting Decision Jam](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/87)


## Week of 2021-11-29
### Priorities
1. ✅ [Synchronously Test PagerDuty Alerts in GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/345331)
1. ✅ Finalize and merge [Monitor's Letter from the Editor](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94572)
1. ❌ Start the [Opportunity Canvas for Error Tracking](https://gitlab.com/gitlab-org/gitlab/-/issues/345058)
1. ❌ Complete the requirements [Error Tracking Instrumentation ](https://gitlab.com/gitlab-org/gitlab/-/issues/345053)
1. ❌ Complete speed runs for Void and Lightstep Incident Response 
1. ✅ Iterate on [UX Roadmap: Incident Management Viable -> Complete](https://gitlab.com/groups/gitlab-org/-/epics/7013)
1. ✅ Iterate on MR for [Removing Product Onboarding template to combine 100d](https://gitlab.com/gitlab-com/Product/-/merge_requests/317)
1. ✅ Complete the Opportunity Mapping Course

## Week of 2021-11-22
### Priorities
NOTE: OOO for Thanksgiving Holiday and Family & Friend's Day 11/25 - 11/29
1. Complete three different speed runs for Grafana OnCall ✅, Void ❌, and Lightstep Incident Response ❌ 
1. ✅ Iterate on [UX Roadmap: Incident Management Viable -> Complete](https://gitlab.com/groups/gitlab-org/-/epics/7013)
1. ✅ Iterate on MR for [Removing Product Onboarding template to combine 100d](https://gitlab.com/gitlab-com/Product/-/merge_requests/317)
1. ✅ Catch up on Opportunity Mapping Course
1. ✅ Learn about the [Corrective Action Items](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/562) backlog and process

## Week of 2021-11-15
### Priorities
NOTE: On PTO 11/10 - 11/16
1. ✅ Make sure all Release Post Items for 14.5 are ready and get merged
1. ✅ PI Updates
1. ✅ Week 4 of Opportunity Mapping Course

## Week of 2021-11-08
### Priorities
1. ✅ Refine Release Post Items for 14.5
1. ✅ Finalize [14.6 Planning Issue](https://gitlab.com/gitlab-org/monitor/monitor/-/issues/89) and Record Kickoff video
1. ✅ Finalize [PM Coverage for Alana Bellucci from 2021-11-10 until 2021-11-16](https://gitlab.com/gitlab-com/Product/-/issues/3332)
1. ✅ Complete [Validate requirements for an internal analytics dashboard for the Infrastructure team for Incidents](https://gitlab.com/gitlab-org/gitlab/-/issues/342571) and determine a synchronous time to test GitLab Alerts for Dogfooding.
1. ✅ Week 3 of Opportunity Mapping Course

## Week of 2021-11-01
### Priorities
1. ✅ Complete near team, mid-term and long term strategy for Monitor, [MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92266)
1. ✅ Complete [Slack and incident management workflow problem validation](https://gitlab.com/gitlab-org/gitlab/-/issues/333030) 
1. ❌ Complete [Validate requirements for an internal analytics dashboard for the Infrastructure team for Incidents](https://gitlab.com/gitlab-org/gitlab/-/issues/342571) and determine a synchronous time to test GitLab Alerts for Dogfooding.
1. ❌ Speed Runs for [Void](https://gitlab.com/gitlab-com/Product/-/issues/3224) and [Lightstep Incident Response](https://gitlab.com/gitlab-com/Product/-/issues/3223) 
1. ✅ Iterate on [UX Roadmap: Incident Management Viable -> Complete](https://gitlab.com/groups/gitlab-org/-/epics/7013)
1. ✅ Week 2 of Opportunity Mapping Course