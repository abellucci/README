---
layout: markdown_page
title: "Alana Bellucci's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY Q2 2023

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Monitor | Incident Management specific | 60% |
| Monitor | Getting familiar with other categories and stages at GitLab | 10% |
| Synchronous Meetings | Coffee chats, team meetings, AMA | 10% |
| Sensing Mechanisms | Customer interviews, competition | 10% |
| Personal Development | writing, reading, learning | 10% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

Priorities are set on Monday and reviewed at EOD on Fridays:
 - ✅ Successfully completed
 - ❌ Did not complete
 - 🟡 Partially completed

## Week of 2022-07-25
### Priorities
_Week 2 returning from parental leave, working at 50% capacity_
1. ✅ Finalize [FY2023 Q3 Goals/OKRs ](https://gitlab.com/gitlab-org/monitor/respond/-/issues/136)
1. ✅ Review [Problem Validation: Incident Retrospectives](https://dovetailapp.com/projects/5AfI3d15GxVCV5kBy6LiGM/readme)
1. ✅ Review Competitve Analysis for [Blameless](https://gitlab.com/gitlab-org/monitor/respond/-/issues/119) and [Firehydrant.io](https://gitlab.com/gitlab-org/monitor/respond/-/issues/117)
1. ✅ Iterate with Nicole on the new [Respond Group Dashboard](https://gitlab.com/gitlab-org/monitor/respond/-/issues/119)
1. ❌ Review the [Incident Management Slack App](https://gitlab.com/gitlab-org/gitlab/-/issues/359322#note_1035993839)

## Week of 2022-07-18
### Priorities
_Week 1 returning from parental leave, working at 50% capacity_
1. ✅ Meet with Kevin, Nicole, Amelia and Crystal to touch base and catch up 
1. ✅ Review [Weekly Breakdown](https://gitlab.com/kbychu/README/-/blob/master/alana-leave-weekly-breadown.md)
1. ✅ Organize emails and start Todos