---
layout: markdown_page
title: "Alana Bellucci's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY2023 Q4

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Monitor | Incident Management specific | 60% |
| Monitor | Getting familiar with other categories and stages at GitLab | 10% |
| Synchronous Meetings | Coffee chats, team meetings, AMA | 10% |
| Sensing Mechanisms | Customer interviews, competition | 10% |
| Personal Development | writing, reading, learning | 10% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

Priorities are set on Monday and reviewed at EOD on Fridays:
 - ✅ Successfully completed
 - ❌ Did not complete
 - 🟡 Partially completed

# Week of 2022-10-24
1. 🟡 [Revert Prometheus Alerts Documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/373301)
1. ❌ [Add additional logos to Linked Resources](https://gitlab.com/gitlab-org/gitlab/-/issues/371210)
1. 🟡 [Improve text/docs around required permissions for system updates to alert status](https://gitlab.com/gitlab-org/gitlab/-/issues/347500)
1. ❌ Finalize [Monitor:Respond - FY23Q4 Goals](https://gitlab.com/gitlab-org/monitor/respond/-/issues/162)
1. ✅ Record monthly demo video for Incident Management Direction Page
1. ✅ Complete first draft of the [Solutions Brief for Incident Management](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/6669)
1. ✅ Draft requirements for [CI/CD Alerts](https://gitlab.com/gitlab-org/gitlab/-/issues/217770)

# Week of 2022-10-17
1. ✅ Release Post Manager Shadow
1. ✅ Start [Incident Management Metrics: A Visual Guide Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/111658)
1. ✅ Publish [Incident Timelines Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/111657)
1. ✅ Review Performance Indicators
1. ✅ Incorporate UX Roadmap into Incident Management Direction page with [new guidelines](https://gitlab.com/gitlab-com/Product/-/issues/4818)
1. ✅ Start [Monitor:Respond UX Roadmap for FY2024](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2129)

# Week of 2022-10-10
_Short week with US Holiday on Monday_
### Priorities 
1. ✅ CAB Presentation 
1. 🟡 Publish Incident Timelines Blog Post
1. ✅ Complete 15.6 planning and record kickoff video
1. ✅ Release Post Manager Shadow
1. ✅ Open issues and identify requirements for Product Led Growth Opportunities

# Week of 2022-10-03
_Short week with PTO Monday - Tuesday and Friends and Family Day Wednesday_
### Priorities 
1. ❌ Open issues and identify requirements for Product Led Growth Opportunities
1. ✅ Incident Timelines Blog Post Revisions 
1. ✅ CAB Presentation Prep

# Week of 2022-09-26
### Priorities 
_Short week with speaking course, Friends and Family Day, and PTO_
1. ✅ [Communicate to Influence Course](https://decker.com/courses/communicate-to-influence/)
1. ✅ Complete direction updates
1. ✅ Incident Timelines Blog Post Revisions 

# Week of 2022-09-19
### Priorities 
1. ✅ Draft outline for [Incident Management Blog Post Series](https://gitlab.com/groups/gitlab-org/monitor/-/epics/2)
1. ✅ Start draft for first blog post
1. ✅ Complete Performance Indicator review
1. ❌ Open issues and identify requirements for Product Led Growth Opportunities
1. ✅ October CAB Prep

# Week of 2022-09-12
### Priorities
1. ✅ September Product Direction Showcase
1. ✅ Finalize all Release Post Items for 15.5
1. ✅ Finalize [Monitor:Respond 15.5 Planning Issue](https://gitlab.com/gitlab-org/monitor/respond/-/issues/152) and record kickoff video
1. ✅ Work with Nicole to [Reevaluate Respond Group GMAU](https://gitlab.com/gitlab-org/monitor/respond/-/issues/145)
1. 🟡 Meet with other Product Managers to [Brainstorm - Product Led Growth for Monitor:Respond](https://gitlab.com/gitlab-org/monitor/respond/-/issues/144)
1. ❌ Draft outline for [Incident Management Blog Post Series](https://gitlab.com/groups/gitlab-org/monitor/-/epics/2)
1. 🟡 Start working with legal to [add additional logos as SVGs for Linked Resources](https://gitlab.com/gitlab-org/gitlab/-/issues/371210#note_1087188856)

# Week of 2022-09-05
### Priorities
_One day week with Labor Day on Monday and PTO Tuesday-Thursday_
1. ✅ ToDos!

# Week of 2022-08-29
### Priorities
1. ✅ Complete Direction Updates for August
1. ✅ Make updates to Monitor:Respond Planning Issue template
1. ✅ [Synchronous test of associating GitLab Alerts to GitLab Incidents](https://gitlab.com/gitlab-org/monitor/respond/-/issues/149)
1. 🟡 [Reevaluate Respond Group GMAU](https://gitlab.com/gitlab-org/monitor/respond/-/issues/145)
1. ✅ [Brainstorm - Product Led Growth for Monitor:Respond](https://gitlab.com/gitlab-org/monitor/respond/-/issues/144)
1. 🟡 Research topics for [Incident Management Blog Post Series](https://gitlab.com/gitlab-org/monitor/respond/-/issues/140#:~:text=Incident%20Management%20Blog%20Post%20Series)

# Week of 2022-08-22
### Priorities
1. 🟡 [Reevaluate Respond Group GMAU](https://gitlab.com/gitlab-org/monitor/respond/-/issues/145)
1. 🟡 [Brainstorm - Product Led Growth for Monitor:Respond](https://gitlab.com/gitlab-org/monitor/respond/-/issues/144)
1. 🟡 Research topics for [Incident Management Blog Post Series](https://gitlab.com/gitlab-org/monitor/respond/-/issues/140#:~:text=Incident%20Management%20Blog%20Post%20Series)

# Week of 2022-08-15
### Priorities
1. ✅ Start updating the [Performance Indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/108670) section of the Monitor:Respond Engineering page
1. ✅ Start a brainstorming issue for product led growth for Respond
1. ✅ Consider alternatives for Monitor:Respond GMAU
1. ✅ Start outline for incident response blog post series
1. ✅ Connect with Grant about the GitLab Slack App
1. ✅ Record a speed run for the designs for the Slack App, included as a part of the [15.4 kickoff video ](https://youtu.be/CuY1Wo06jhE), (2:00-4:50).
1. ✅ Sign up for [Communicate to Influence](https://decker.com/courses/communicate-to-influence/) course
1. ✅ Suggest updates to Deep Dive interview template
1. ✅ Meet with team members to brainstorm how to best link alerts to incidents in GitLab
1. ✅ Followup on Dogfooding issues

# Week of 2022-08-08
### Priorities
1. ✅ Complete planning issue for 15.4 and record kickoff video
1. ✅ Complete drafts for 15.3 Release Post Items
1. ✅ Complete tasks to facilitate dogfooding for Linked Resources 
1. ✅ Sign up for the next Product Direction Showcase (September)
1. ❌ Start updating the [Performance Indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/108670) section of the Monitor:Respond Engineering page
1. ❌ Start a brainstorming issue for product led growth for Respond
 
## Week of 2022-08-01
### Priorities
1. ✅ Review the [Incident Management Slack App](https://gitlab.com/gitlab-org/gitlab/-/issues/359322#note_1035993839)
1. ✅ Complete July [Direction Updates for Monitor:Respond](https://gitlab.com/gitlab-com/Product/-/issues/4537)
1. ✅ Write 500 word blurb for [Product at GitLab LinkedIn Life Page Buildout](https://gitlab.com/gitlab-com/people-group/talent-brand-and-engagement/-/issues/45)
1. ✅ Schedule 1-2 internal interviews for [Problem Validation: How do past incidents inform future incident response?](https://gitlab.com/gitlab-org/ux-research/-/issues/1925)
1. ❌ Start updating the [Performance Indicator](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/108670) section of the Monitor:Respond Engineering page
1. ✅ Complete Security Training