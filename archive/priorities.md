---
layout: markdown_page
title: "Alana Bellucci's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY2023 Q4

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Monitor | Incident Management specific | 60% |
| Monitor | Getting familiar with other categories and stages at GitLab | 10% |
| Synchronous Meetings | Coffee chats, team meetings, AMA | 10% |
| Sensing Mechanisms | Customer interviews, competition | 10% |
| Personal Development | writing, reading, learning | 10% |

Priorities are set on Monday and reviewed at EOD on Fridays:
 - ✅ Successfully completed
 - ❌ Did not complete
 - 🟡 Partially completed

## Future Priorities
### Week of 2023-01-30
_End of Quarter_
1. Finalize OKRs for FY2024-Q1
1. Write requirements for third party paging service
1. Make suggestions to the Marketing competition pages
1. Setup new computer
1. Kick off discussion for PM Standup adoption
1. Review performance indicators for Slack adoption and on-call schedule management

## Next Priorities
### Week of 2023-01-23
1. Kickoff Slack App for Incident Management Launch
1. Setup a Problem Validation and/or determine requirements for the Slack App for Service Desk
1. Setup a Problem Validation and/or determine the requirements for Service Level Agreements for Service Desk
1. Complete strategy for ways to [talk to more customers](https://gitlab.com/gitlab-org/monitor/respond/-/issues/176) and start screeners and interview questions
1. Reconcile 15.9 planning issue with boards for Incident Management and Service Desk
1. Start and finish all [Gibson Biddle](https://gitlab.com/gitlab-org/monitor/respond/-/issues/166) exercise
1. ✅ Restart [Re-introduce documentation for Prometheus alert integrations](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/102711)

### Week of 2023-01-16
_Short week due to MLK day and Friends and Family Day_
1. ✅ Performance indicator review
1. ✅ Release post items
1. ✅ Service desk planning issue and board setup
1. ✅ Finalize all Service Desk PM transition items

### Week of 2023-01-09
1. ✅ Continue to on-board as the new PM for Service Desk
1. ✅ Finalize [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/5230)
1. 🟡 Complete strategy for ways to [talk to more customers](https://gitlab.com/gitlab-org/monitor/respond/-/issues/176)
1. ✅ 15.9 Planning and Kickoff
1. ✅ Finalize Incident Management [Product Teardown](https://userguiding.com/blog/slack-user-onboarding-teardown/)
1. ❌ [Gibson Biddle](https://gitlab.com/gitlab-org/monitor/respond/-/issues/166) - [From DHM to Product Strategy](https://gitlab.com/gitlab-org/monitor/respond/-/issues/166#note_1146906526)

### Week of 2023-01-02
1. ✅ Gain an understanding of the state of GitLab Service Desk
1. ✅ Start [Direction Updates](https://gitlab.com/gitlab-com/Product/-/issues/5230)
1. 🟡 Find more ways to [talk to customers](https://gitlab.com/gitlab-org/monitor/respond/-/issues/176)
1. ✅ Start Incident Management [Product Teardown](https://userguiding.com/blog/slack-user-onboarding-teardown/)
1. ✅ [Gibson Biddle](https://gitlab.com/gitlab-org/monitor/respond/-/issues/166) - [The DHM Model](https://gitlab.com/gitlab-org/monitor/respond/-/issues/166#note_1146906119)

### Week of 2022-12-26
_Out of Office_

### Week of 2022-12-19
🌡️ _Sick most of the week, caught up_

### Week of 2022-12-12
🌡️ _Sick_
1. ✅ Draft and finalize planning issue
1. ❌ Make suggestions to the Marketing competition pages
1. ❌ Start the next blog post
1. ❌ Draft SLA requirements
1. ❌ Update OKRs

### Week of 2022-12-05
🌡️ _Sick_
1. 🟡 Finalize and publish the [Incident Management Metrics: A Visual Guide](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/111658) blog post
1. 🟡 Find more ways to talk to customers
1. ❌ Test [Slack App](https://docs.gitlab.com/ee/operations/incident_management/slack.html) and create feedback issue
1. ✅ [GMAU action items](https://gitlab.com/gitlab-org/gitlab/-/issues/382988#note_1190060872)
1. ❌ [Gibson Biddle Exercises](https://gitlab.com/gitlab-org/monitor/respond/-/issues/166)
1. ❌ Re-vamp Problem Validations

### Week of 2022-11-28
1. 🟡 Finalize and publish the [Incident Management Metrics: A Visual Guide](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/111658) blog post
1. ✅ Revisit Jobs To Be Done (JTBD) for Incident Management 
1. ✅ Setup [individual metrics that are included in Respond Group GMAU](https://gitlab.com/gitlab-org/gitlab/-/issues/382988)
1. ✅ Direction Updates
1. ✅ Dogfood Linked Resources - show examples and post to infrastructure channels

### Week of 2022-11-21
_Short week due to Thanksgiving holiday_
1. ✅ Release post manager tasks

### Week of 2022-11-14
1. ✅ Release post manager tasks
1. ✅ Finish 15.7 Planning issue and record kickoff
1. ✅ Complete Performance Indicator Review
1. ✅ DevGuild Incident Response Conference
1. ✅ Finalize Goals for Q4 FY2023

### Week of 2022-11-07
1. ✅ Release post manager tasks
1. ✅ Finalize Respond Group's Release Post Items for 15.6
1. 🟡 Start 15.7 Planning
1. ❌ Create a discussion guide and recruiting request issue for the Problem Validation for _[How do I find out who can help when there is an incident?](https://gitlab.com/gitlab-org/ux-research/-/issues/2155)_
1. 🟡 Finalize Goals for Q4 FY2023

### Week of 2022-10-31
1. ✅ Archive and start a new priorities list and improvement plan for the quarter
1. ✅ Complete annual talent assessment
1. ✅ Start working on Problem Validation for _[How do I find out who can help when there is an incident?](https://gitlab.com/gitlab-org/ux-research/-/issues/2155)_
1. 🟡 Start working on Problem Validation for _[How are teams using GitLab or not to gate deployment or do post-deployment follow-up activities?](https://gitlab.com/gitlab-org/ux-research/-/issues/2155)_
1. ✅ Complete the first draft of [Incident Management Metrics: A Visual Guide](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/111658)
1. 🟡 Finalize Goals for Q4 FY2023
