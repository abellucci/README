---
layout: markdown_page
title: "Alana Bellucci's Weekly Snippets"
---

# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY Q1 2023

| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Monitor | Incident Management specific | 60% |
| Monitor | Getting familiar with other categories and stages at GitLab | 10% |
| Synchronous Meetings | Coffee chats, team meetings, AMA | 10% |
| Sensing Mechanisms | Customer interviews, competition | 10% |
| Personal Development | writing, reading, learning | 10% |

# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

Priorities are set on Monday and reviewed at EOD on Fridays:
 - ✅ Successfully completed
 - ❌ Did not complete
 - 🟡 Partially completed

## Week of 2022-03-21
### Priorities
1. ✅ Finalize [Coverage Issue](https://gitlab.com/gitlab-com/Product/-/issues/3802)
1. ✅ Hand over anything that will take longer than a week to Kevin
1. ✅ Draft planning issues for 15.1, 15.2

## Week of 2022-03-14
### Priorities
1. ✅ Finalize [Monitor:Respond 14.10 Planning Issue](https://gitlab.com/gitlab-org/monitor/respond/-/issues/102) and record kickoff video
1. ✅ Draft and merge Monitor:Respond Performance Indicator Review for March
1. ✅ Finalize insights for the [Problem Validation: Continuous Verification](https://gitlab.com/gitlab-org/ux-research/-/issues/1748)
1. ✅ Create slides and present at the Product Direction Showcase
1. ❌ Draft planning issues for 15.1, 15.2
1. ❌ Add and merge the Analyst Landscape for the Continuous Verification Direction page
1. ❌ Create the JTBD section for the Continuous Verification category

## Week of 2022-03-07
### Priorities
1. ✅ Finalize [Analyst Feedback and make updates to the Continuous Verification Direction Page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100062)
1. ✅ Complete all interviews for [Problem Validation: Continuous Verification](https://gitlab.com/gitlab-org/ux-research/-/issues/1748) and start analyzing results in Dovetail
1. ✅ Determine next steps for communicating with users about the [feature flag for removing Integrated Error Tracking by default](https://gitlab.com/gitlab-org/gitlab/-/issues/353639)
1. ✅ Prep release notes for review for 14.9
1. ❌ Draft planning issues for 15.1, 15.2
1. ✅ Create a speed run of [Spike.sh](https://spike.sh/) 

## Week of 2022-02-28
### Priorities
1. ✅ Finalize and merge [Strategy for Incident Management](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99622)
1. ✅ Finalize and merge [Overview for Error Tracking](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99718)
1. ❌ Create speed runs of [Spike.sh](https://spike.sh/) 
1. ✅ Create deprecation and documentation changes for GitLab Self-Monitoring
1. ❌ Create issue for Monitor SMAU instrumentation requirements
1. Draft planning issues for ✅ 14.10, ✅ 15.0, ❌ 15.1, ❌ 15.2

## Week of 2022-02-21
### Priorities
_Three day week with President's Day on Monday and F&F Day on Friday_
1. ✅ Work with leadership and the Respond team to determine next steps for [Error Tracking](https://gitlab.com/groups/gitlab-org/-/epics/7580)
1. ✅ Start February direction updates
1. ✅ Shadow IMOC and CMOC Gameday
1. ❌ Draft planning issues for 14.10, 15.0, 15.1, 15.2

## Week of 2022-02-14
### Priorities
1. ✅ Finalize [Monitor:Respond 14.9 Planning Issue](https://gitlab.com/gitlab-org/monitor/respond/-/issues/101) and record kickoff video
1. ✅ Finalize [14.8 Release Post Items](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name[]=release%20post%20item&label_name[]=group%3A%3Arespond)
1. ✅ Attend and review key takeaways for two analyst calls
1. ✅ Get final approval and merge [Removing the Synthetic Monitoring Category](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98169)
1. ✅ Finalize and merge [Deprecation MR for Monitor:Self Monitoring](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/80535)

## Week of 2022-02-07
### Priorities
1. ✅ Start Planning issue for 14.9
1. ✅ Update and organize issue boards: [Monitor:Respond - Next 3 Milestones](https://gitlab.com/groups/gitlab-org/-/boards/3370429?label_name[]=group%3A%3Arespond) and [Monitor:Respond - Quarterly Direction (Epics)](https://gitlab.com/groups/gitlab-org/-/epic_boards/9486?label_name[]=group::respond)
1. ✅ Build out screener in Qualtrics for the [Problem Validation: Continuous Verification](https://gitlab.com/gitlab-org/ux-research/-/issues/1748)
1. ✅ Start 14.8 Release Post Items
1. 🟡 Finalize and merge [Removing the Synthetic Monitoring Category](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/98169)
1. ✅ Finalize and merge [Self-Monitoring DEPRECATED documentation changes](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76891)

## Week of 2022-01-31
### Priorities
1. ✅ Finalize and merge [Continuous Verification Direction Page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97512)
1. ✅ Finalize and merge [Continuous Verification Category Addition](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97539)
1. ✅ Finalize and merge [Incident Management features the Infrastructure team is currently dogfooding](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/97289)
1. ✅ Start removal entries for [15.0 Monitor Stage Deprecations](https://gitlab.com/groups/gitlab-org/-/epics/7188)
1. ✅ Finalize screener and discussion guide for [Problem Validation: Continuous Verification](https://gitlab.com/gitlab-org/ux-research/-/issues/1748)
