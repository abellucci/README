---
layout: markdown_page
title: "Alana Bellucci's README"
description: "Alana is a Product Manager at GitLab. This page is a little about her.
---

# Alana's README

Hello! I am Alana and I'm a [Senior Product Manager](https://about.gitlab.com/job-families/product/product-manager/) for Threat Insights. This README is meant to tell you a bit more about myself and to provide a glimpse to what it might be like to work with me. Please feel free to contribute to this page by opening a merge request.

* [GitLab Handle](https://gitlab.com/abellucci)
* [Team Page](https://about.gitlab.com/company/team/#abellucci) 

# About me

* I live in Boulder, Colorado with my husband and daughter.
* I like to start my work day around 9am MST. 

# Personality

## Meyer's Briggs
* 	INFJ - Introvert, Inutitive, Feeling, and Judging.  This personailty type is considered an Advocate.  Click [here ](https://www.16personalities.com/infj-personality) to learn more about INFJ's.


## Strengths Finder
* [Strategic](https://www.gallup.com/cliftonstrengths/en/252350/strategic-theme.aspx)
* [Communication](https://www.gallup.com/cliftonstrengths/en/252185/communication-theme.aspx)
* [Ideation](https://www.gallup.com/cliftonstrengths/en/252260/ideation-theme.aspx)
* [Realtor](https://www.gallup.com/cliftonstrengths/en/252311/relator-theme.aspx)
* [Focus](https://www.gallup.com/cliftonstrengths/en/252239/focus-theme.aspx)

